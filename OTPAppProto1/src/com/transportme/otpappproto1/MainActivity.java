package com.transportme.otpappproto1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.ResourceProxy;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.OverlayItem.HotspotPlace;
import org.osmdroid.views.overlay.PathOverlay;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

/**

C�DIGO MERAMENTE EXEMPLIFICATIVO DO USO 
DA API DO OTP E DO OSMDROID.

N�O FORAM TOMADOS EM CONTA ALGUNS CUIDADOS 
IMPORTANTES COMO FAZER OS REQUESTS � API 
NUMA THREAD DIFERENTE DA UI THREAD.

**/
public class MainActivity extends Activity implements
OnDateSetListener, OnTimeSetListener, OnClickListener{

	private int year, month, day, hour, minute; 
	private int mYear, mMonth, mDay, mHour, mMinute;
	private double startLat, startLon;
	private double endLat, endLon;
	private static final int DATE_DIALOG_ID = 0;
	private static final int TIME_DIALOG_ID = 1;
	private static final int DIALOG_PROMPT = 2;
	private String fromPlace,toPlace;
	private EditText eOrigin,eDestination;
	MetadataInfo OTPMetadata ;
	
	private Calendar c;
	
	private static final String API_ORIGIN = "fromPlace";
	private static final String API_DESTINATION = "toPlace";
	private static final String API_MODE= "mode";
	private static final String API_OPTIMIZE = "optimize";
	private static final String API_WALKSPEED = "walkSpeed";
	private static final String API_ARRIVE_BY = "arriveBy";
	private static final String API_TIME= "time";
	private static final String API_DATE= "date";
	private static final String API_MAX_WALK_DISTANCE= "maxWalkDistance";
	
	private static final String GEOCODE_ADDRESS= "address";
	private static final String GEOCODE_BOUNDING_BOX= "bbox";
	
	private static final String TAG= "OTP";
	
	private MapView mv;
	private MapController mc;
    private ItemizedOverlay<OverlayItem> mMyLocationOverlay;
    private ResourceProxy mResourceProxy;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mResourceProxy = new DefaultResourceProxyImpl(getApplicationContext());
		setContentView(R.layout.activity_main);
		
		c = Calendar.getInstance();
		year = month = day = hour = minute = 1;
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		mHour = c.get(Calendar.HOUR_OF_DAY);
		mMinute = c.get(Calendar.MINUTE);
		
		mv = (MapView) findViewById(R.id.mapview);
		mv.setBuiltInZoomControls(true);
		mv.setMultiTouchControls(true);
		mc = mv.getController();
		mc.setZoom(10);
		//Faz um pedido � API para receber a metadata, que cont�m a informa��o
		//dos limites do mapa e do centro do mesmo.
		OTPMetadata = getMetadata();
		//Define o centro do mapa
		mc.setCenter(new GeoPoint(OTPMetadata.getCenterLatitude(), OTPMetadata.getCenterLongitude()));
		
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.data:
			showDialog(DATE_DIALOG_ID);
			return true;

		case R.id.hora:
			showDialog(TIME_DIALOG_ID);
			return true;

		case R.id.Percurso:
			showDialog(DIALOG_PROMPT);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, this, mYear, mMonth, mDay);
		case TIME_DIALOG_ID:
			return new TimePickerDialog(this, this, mHour, mMinute, false);
		case DIALOG_PROMPT:
			// Create out AlterDialog
			LayoutInflater li = LayoutInflater.from(this);
			View promptView = li.inflate(R.layout.prompt, null);
			eOrigin = (EditText) promptView.findViewById(R.id.editTextOrigin);
			eDestination = (EditText) promptView.findViewById(R.id.editTextDestiny);
			Builder builder = new AlertDialog.Builder(this);
			builder.setView(promptView);
			builder.setCancelable(true);
			builder.setPositiveButton("ok", this);
			AlertDialog dialog = builder.create();
			dialog.show();
		}
		return null;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onDateSet(DatePicker view, int yearSelected, int monthOfYear,
			int dayOfMonth) {
		year = yearSelected;
		month = monthOfYear;
		day = dayOfMonth;
		c.set(Calendar.YEAR, year);
		c.set(Calendar.DAY_OF_MONTH, day);
		c.set(Calendar.MONTH, month);
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int min) {
		hour = hourOfDay;
		minute = min;
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, minute);
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		JSONObject json;
		JSONObject firstStop;
		JSONObject lastStop;
		JSONArray arr;
		GeoPoint firstStopGeoPoint = null, lastStopGeoPoint = null;
		StopInfo firstStopInfo = null, lastStopInfo = null;
		
		try {
			json = readTransports().getJSONObject("plan");
			arr = json.getJSONArray("itineraries");
			//Ir� ser escolhido o primeiro itiner�rio
			arr = arr.getJSONObject(0).getJSONArray("legs");
			//De todos os excertos (legs) do itiner�rio, o primeiro excerto do percurso � o caminho a p� desde 
			//as coordenadas definidas para o ponto de partida, at� � primeira paragem/esta��o, e o �ltimo excerto � o 
			//caminho a p� desde a �ltima paragem at� �s coordenadas da chegada. Como queremos a informa��o da primeira 
			//e �ltima paragem/esta��o, queremos aceder ao 2� e ao pen�ltimo excertos. se s� houver um excerto, ent�o �
			//porque todo o percurso � a p�, n�o usando portanto os transportes p�blicos
			if(arr.length() == 1){
				firstStopInfo = new StopInfo();
				firstStopInfo.setTitle("Percurso a P�");
				firstStopInfo.setDescription(fromPlace);
				firstStopInfo.setLatitude(startLat);
				firstStopInfo.setLongitude(startLon);
				
				lastStopInfo = new StopInfo();
				lastStopInfo.setTitle("Percurso a P�");
				lastStopInfo.setDescription(toPlace);
				lastStopInfo.setLatitude(endLat);
				lastStopInfo.setLongitude(endLon);
				
			}else{
				firstStop = arr.getJSONObject(1);
				lastStop = arr.getJSONObject(arr.length()-2);
				
				firstStopInfo = getStopInfo(firstStop, true);
				firstStopGeoPoint = new GeoPoint(firstStopInfo.getLatitude(), firstStopInfo.getLongitude());
				
				lastStopInfo = getStopInfo(lastStop, false);
				lastStopGeoPoint = new GeoPoint(lastStopInfo.getLatitude(), lastStopInfo.getLongitude());
			}
			
			ArrayList<OverlayItem> items = new ArrayList<OverlayItem>();
			
			//Adiciona um pin na primeira paragem do percurso e um pin na �ltima
			OverlayItem startOverlay = new OverlayItem(firstStopInfo.getTitle(), firstStopInfo.getDescription(), firstStopGeoPoint);
		 	OverlayItem endOverlay = new OverlayItem(lastStopInfo.getTitle(), lastStopInfo.getDescription(), lastStopGeoPoint);
		 	Drawable startMarker = getResources().getDrawable(R.drawable.start);
		 	Drawable endMarker = getResources().getDrawable(R.drawable.end);
		 	startOverlay.setMarker(startMarker);
		 	startOverlay.setMarkerHotspot(HotspotPlace.BOTTOM_CENTER);
		 	endOverlay.setMarker(endMarker);
		 	endOverlay.setMarkerHotspot(HotspotPlace.BOTTOM_CENTER);
	        items.add(startOverlay);
	        items.add(endOverlay);
			
			//Desenha uma linha recta do ponto de partida � primeira paragem,
			//da primeira paragem � �ltima paragem e da �ltima paragem ao
			//ponto de chegada.
		    PathOverlay myPath = new PathOverlay(Color.RED, this);
		    myPath.addPoint(new GeoPoint(startLat, startLon));
		    myPath.addPoint(firstStopGeoPoint);
		    myPath.addPoint(lastStopGeoPoint);
		    myPath.addPoint(new GeoPoint(endLat, endLon));
		    
		    mv.getOverlays().add(myPath);
		    mc.setCenter(firstStopGeoPoint);
		    mc.setZoom(15);
		    
			//Comportamento ao clicar no pin da paragem
		    this.mMyLocationOverlay = new ItemizedIconOverlay<OverlayItem>(items,
	                new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
	                    @Override
	                    public boolean onItemSingleTapUp(final int index,
	                            final OverlayItem item) {
	                    	
	                        Toast.makeText(
	                        		MainActivity.this,
	                                item.mTitle +"\n"+item.mDescription, Toast.LENGTH_LONG
	                        ).show();
	                        
	                        return true;
	                    }
	                    @Override
	                    public boolean onItemLongPress(final int index, final OverlayItem item) {
	                        return false;
	                    }
	                }, mResourceProxy);
	        mv.getOverlays().add(mMyLocationOverlay);
	        mv.invalidate();
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		
		

	}
	
	/***************************************************************************/
	/*************************** CUSTOM METHODS	********************************/
	/***************************************************************************/
	
	private StopInfo getStopInfo(JSONObject stop, boolean startStop) {
		StopInfo stopInfo = new StopInfo();
		JSONObject json;
		try {
			json = stop.getJSONObject(startStop ? "from" : "to");
			stopInfo.setTitle(json.getJSONObject("stopId").getString("agencyId") +
					 " - " + stop.getString("mode") + " " + stop.getString("route"));
			stopInfo.setDescription(json.getString("name"));
			
			stopInfo.setLatitude(json.getDouble("lat"));
			stopInfo.setLongitude(json.getDouble("lon"));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		return stopInfo;
	}
	
	private String composeURI(String uri, HashMap<String, String> params){
		if(params == null) return uri;
		StringBuilder builder = new StringBuilder();
		builder.append(uri);
		Iterator<Entry<String, String>> i = params.entrySet().iterator();
		while(i.hasNext()) {
			Entry<String, String> param = i.next();
	    	builder.append("&"+param.getKey()+"="+param.getValue());
	    }
		String uriComposed = builder.toString();
		Log.d(TAG, uriComposed);
		return uriComposed;
	}
	
	
	private JSONObject readTransports() {
		fromPlace = eOrigin.getText().toString().replace(" ", "%20");
		toPlace = eDestination.getText().toString().replace(" ", "%20");

		HashMap<String, Double> origin = getCoordsFromAddress(fromPlace);
		startLat = origin.get("lat");
		startLon = origin.get("lon");
		HashMap<String, Double> destination = getCoordsFromAddress(toPlace);
		endLat = destination.get("lat");
		endLon = destination.get("lon");
		
		fromPlace = Double.toString(origin.get("lat"))+"%2C"+Double.toString(origin.get("lon"));
		toPlace = Double.toString(destination.get("lat"))+"%2C"+Double.toString(destination.get("lon"));
		final 
		
		String uri = composeURI(
				"http://ec2-50-16-72-104.compute-1.amazonaws.com:8080/opentripplanner-api-webapp/ws/plan?",

				new HashMap<String, String>(){
					{
						put(API_DESTINATION, toPlace);
						put(API_ORIGIN, fromPlace);
						put(API_MODE, "TRANSIT%2CWALK");
						put(API_OPTIMIZE, "QUICK");
						put(API_ARRIVE_BY, "false");
						put(API_WALKSPEED, "0.833");
						put(API_DATE, year+"-"+month+"-"+day);
						put(API_TIME, hour+"%3A"+minute);
					}
				}
		);
		
		return getRequestInfoInJSON(uri);
	}
	
	private HashMap<String, Double> getCoordsFromAddress(final String address){
		HashMap<String, Double> result = new HashMap<String, Double>();
		
		JSONObject coords = getRequestInfoInJSON(
				composeURI("http://ec2-50-16-72-104.compute-1.amazonaws.com:8080/opentripplanner-geocoder/geocode?",
						new HashMap<String, String>(){
							{
								put(GEOCODE_ADDRESS, address);
								put(GEOCODE_BOUNDING_BOX, 
										OTPMetadata.getMinLongitude()+","+
										OTPMetadata.getMaxLatitude()+","+
										OTPMetadata.getMaxLongitude()+","+
										OTPMetadata.getMinLatitude());
							}
						}
				)
		);
		
		try {
			coords = coords.getJSONArray("results").getJSONObject(0);
			result.put("lon", coords.getDouble("lng"));
			result.put("lat", coords.getDouble("lat"));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return result;
		
	}
	
	private JSONObject getRequestInfoInJSON(String uri){
		HttpClient client = new DefaultHttpClient();
		StringBuilder builder = new StringBuilder();
		JSONObject jObj = null;
		HttpGet httpGet = new HttpGet(uri);
		httpGet.addHeader(new BasicHeader("Accept", "application/json"));
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.e(TAG, "Failed to Request");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// tenta fazer parse da string para um objecto json
        try {
            jObj = new JSONObject(builder.toString());
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing data " + e.toString());
        }
 
        return jObj;
	}
	
	private MetadataInfo getMetadata() {
		MetadataInfo md = new MetadataInfo();
		 JSONObject jsonMetadata = getRequestInfoInJSON(
			"http://ec2-50-16-72-104.compute-1.amazonaws.com:8080/opentripplanner-api-webapp/ws/metadata"
		);
		try{
			md.setCenterLatitude(jsonMetadata.getDouble("centerLatitude"));
			md.setCenterLongitude(jsonMetadata.getDouble("centerLongitude"));
			md.setMaxLatitude(jsonMetadata.getDouble("maxLatitude"));
			md.setMaxLongitude(jsonMetadata.getDouble("maxLongitude"));
			md.setMinLatitude(jsonMetadata.getDouble("minLatitude"));
			md.setMinLongitude(jsonMetadata.getDouble("minLongitude"));
			md.setUpperRightLatitude(jsonMetadata.getDouble("upperRightLatitude"));
			md.setUpperRightLongitude(jsonMetadata.getDouble("upperRightLongitude"));
			JSONArray transitModes = jsonMetadata.getJSONArray("transitModes");
			for(int i = 0; i < transitModes.length(); ++i){
				md.addTransitMode(transitModes.getString(i));
			}

	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
		
		return md;
	}
	
	private class StopInfo{
		private String _title;
		private String _desc;
		private double _lat, _lon;
		
		public String getTitle(){ return _title;}
		public void setTitle(String title){_title = title;}
		
		public String getDescription(){ return _desc;}
		public void setDescription(String description){_desc = description;}
		
		public double getLatitude(){ return _lat;}
		public void setLatitude(double latitude){_lat = latitude;}
		
		public double getLongitude(){ return _lon;}
		public void setLongitude(double longitude){_lon = longitude;}
	}
	
	private class MetadataInfo{
		private double _centerLatitude;
		private double _centerLongitude;
		private double _lowerLeftLatitude;
		private double _lowerLeftLongitude;
		private double _maxLatitude;
		private double _maxLongitude;
		private double _minLatitude;
		private double _minLongitude;
		private double _upperRightLatitude;
		private double _upperRightLongitude;
		private ArrayList<String> _transitModes = new ArrayList<String>();
		
		public double getCenterLatitude() {
			return _centerLatitude;
		}
		public void setCenterLatitude(double centerLatitude) {
			_centerLatitude = centerLatitude;
		}
		public double getCenterLongitude() {
			return _centerLongitude;
		}
		public void setCenterLongitude(double centerLongitude) {
			_centerLongitude = centerLongitude;
		}
		public double getLowerLeftLongitude() {
			return _lowerLeftLongitude;
		}
		public void setLowerLeftLongitude(double lowerLeftLongitude) {
			_lowerLeftLongitude = lowerLeftLongitude;
		}
		public double getLowerLeftLatitude() {
			return _lowerLeftLatitude;
		}
		public void setLowerLeftLatitude(double lowerLeftLatitude) {
			_lowerLeftLatitude = lowerLeftLatitude;
		}
		public double getMaxLatitude() {
			return _maxLatitude;
		}
		public void setMaxLatitude(double maxLatitude) {
			_maxLatitude = maxLatitude;
		}
		public double getMaxLongitude() {
			return _maxLongitude;
		}
		public void setMaxLongitude(double maxLongitude) {
			_maxLongitude = maxLongitude;
		}
		public double getMinLatitude() {
			return _minLatitude;
		}
		public void setMinLatitude(double minLatitude) {
			_minLatitude = minLatitude;
		}
		public double getMinLongitude() {
			return _minLongitude;
		}
		public void setMinLongitude(double minLongitude) {
			_minLongitude = minLongitude;
		}
		public double getUpperRightLatitude() {
			return _upperRightLatitude;
		}
		public void setUpperRightLatitude(double upperRightLatitude) {
			_upperRightLatitude = upperRightLatitude;
		}
		public double getUpperRightLongitude() {
			return _upperRightLongitude;
		}
		public void setUpperRightLongitude(double upperRightLongitude) {
			_upperRightLongitude = upperRightLongitude;
		}
		public ArrayList<String> getTransitModes() {
			return _transitModes;
		}
		public void setTransitModes(ArrayList<String> transitModes) {
			_transitModes = transitModes;
		}
		public void addTransitMode(String transitMode) {
			_transitModes.add(transitMode);
		}


	}
	
	
}
